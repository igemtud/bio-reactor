import { Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {Dialogmodus, DialogResult, SyringedialogComponent} from './syringedialog/syringedialog.component';


export interface SyringeTableStructure {
  number: number;
  name: string;
  volume: number;
  diameter: number;
}



const ELEMENT_DATA: SyringeTableStructure[] = [
  {number: 1, name: 'BlackSyringe', volume: 10, diameter: 4},
];
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})


export class ConfigurationComponent implements OnInit {
  displayedColumns: string[] = ['number', 'name', 'volume', 'diameter'];
  dataSource: SyringeTableStructure[] = ELEMENT_DATA;

  constructor(
    private http: HttpClient,
    public dialog: MatDialog
  ) {}


  ngOnInit(): void {
  }

  onClickButton() {
    const dialogRef = this.dialog.open(SyringedialogComponent, {
      width: '301px',
      data: {
        sysData: {number: 1, name: 'BlackSyringe', volume: 10, diameter: 4},
        modus: Dialogmodus.ADD,
      }//
    });
    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      console.log('The dialog was closed');
      if (result !== undefined) {
        this.dataSource.push(result.sysData);
        console.log(result);
        this.dataSource = this.dataSource.slice();
      }

    });

    // const data = {number: 1, name: 'BlackSyringe2', volume: 10, diameter: 4};
    // this.dataSource.push(data);
    // hacky

    this.http.put('/api/state/configuration/Syringe', this.dataSource[this.dataSource.length - 1])
      .subscribe((value: any) => {
        console.log(value);
      });
  }


  onClickEditElement(sy: SyringeTableStructure) {
    const index = this.dataSource.findIndex((e: SyringeTableStructure) => {
      return sy === e;
    });
    const temp: SyringeTableStructure = {number: -1, name: '', volume: -1, diameter: -1};
    Object.assign(temp, sy);

    const dialogRef = this.dialog.open(SyringedialogComponent, {
      width: '400px',
      data: {
        sysData: temp,
        modus: Dialogmodus.EDIT,
      }
    });
    dialogRef.afterClosed().subscribe((result: DialogResult) => {
      if (result !== undefined) {
        if (result.delete) {
          this.dataSource = this.dataSource.filter((sys: SyringeTableStructure, i: number) => {
            return index !== i;
          });
        } else {
          this.dataSource[index] = result.sysData;
          this.dataSource = this.dataSource.slice();
        }
      }
    });
  }
}

