import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SyringeTableStructure} from '../configuration.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-syringedialog',
  templateUrl: './syringedialog.component.html',
  styleUrls: ['./syringedialog.component.css']
})
export class SyringedialogComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  fithFormGroup: FormGroup;
  thirdFormGroup: FormGroup;


  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<SyringedialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClickCancel(): void {
    this.dialogRef.close();
  }

  onNoClickDelete(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: [this.data.sysData.number, Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: [this.data.sysData.name, Validators.required]
    });
    this.thirdFormGroup = this.formBuilder.group({
      thirdCtrl: [this.data.sysData.volume, Validators.required]
    });
    this.fithFormGroup = this.formBuilder.group({
      fithCtrl: [this.data.sysData.diameter, Validators.required]
    });
  }

}

export interface DialogData {
  sysData: SyringeTableStructure;
  modus: Dialogmodus;
}
export interface DialogResult {
  sysData: SyringeTableStructure;
  delete: boolean;
}

export enum Dialogmodus {
  EDIT = 0,
  ADD = 1
}
