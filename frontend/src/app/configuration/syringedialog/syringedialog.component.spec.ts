import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyringedialogComponent } from './syringedialog.component';

describe('SyringedialogComponent', () => {
  let component: SyringedialogComponent;
  let fixture: ComponentFixture<SyringedialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyringedialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyringedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
