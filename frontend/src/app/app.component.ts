import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSliderChange, MatSnackBar} from '@angular/material';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
