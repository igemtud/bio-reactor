import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule,
  MatSliderModule,
  MatSnackBar,
  MatSnackBarModule,
  MatToolbarModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatSelectModule,
  MatTabsModule,
  MatTableModule, MatDialogModule, MatStepperModule, MatCardModule
} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { RouterModule, Router} from '@angular/router';
import { LivecontrolComponent } from './livecontrol/livecontrol.component';
import { SettingsComponent } from './settings/settings.component';
import { MainComponent } from './main/main.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { SyringedialogComponent } from './configuration/syringedialog/syringedialog.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';



@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    LivecontrolComponent,
    SettingsComponent,
    MainComponent,
    ConfigurationComponent,
    SyringedialogComponent,

  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    HttpClientModule,
    MatSliderModule,
    BrowserAnimationsModule,
    FormsModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    RouterModule.forRoot([
      /*{path: '', redirectTo: 'settings'},*/
      {path: '', component: MainComponent},
      {path: 'settings', component: SettingsComponent},
      {path: 'configuration', component: ConfigurationComponent},
      {path: 'livecontrol', component: LivecontrolComponent}
    ]),
    MatSelectModule,
    MatTabsModule,
    MatTableModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatCardModule,
    NgxChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [SyringedialogComponent]
})
export class AppModule { }
