import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSliderChange, MatSnackBar} from '@angular/material';
import {Event, EventsService} from './events.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  motorIds = [0, 1, 2];
  steps: Step[] = [
    ];
  currentStep = -1;

  state: State = State.PAUSE;
  STATE = State;

  sensorData: any;
  chartData: any = [
    {
      name: 'red channel',
      series: [
      ]
    }
    ];


  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private eventsService: EventsService,
  ) {
  }

  ngOnInit(): void {
    // connect to websocket
    const events = this.eventsService.connect('ws://localhost:9400');
    events.subscribe((e) => this.progressEvent(e));

    this.http.get('api/program').subscribe((value: any) => {
      this.steps = value.steps;
      console.log(value);
    });

    this.http.get('api/program/state').subscribe((value: any) => {
      console.log(value);
      this.state = value.state === 'RUNNING' ? State.RUNNING : State.PAUSE;
    });
  }


  progressEvent(event: Event) {
    switch (event.event) {
      case 'odSensor/data':
        if (this.state === State.RUNNING) {
          this.sensorAddData(event.data);
        }
        break;

      case 'program/state':
        this.onProgramStateChange(event.data);
        break;
    }
  }


  counter = 0;
  sensorAddData(data: any) {
    this.sensorData = data;

    this.chartData[0].series.push({
      name: new Date(),
      value: data.red,
    });
    this.chartData = [...this.chartData];

    //this.addData();

    console.log(this.chartData);
  }

  onProgramStateChange(data: any) {
    if (data.state !== 'step') {
      this.currentStep = -1;
      return;
    }

    this.currentStep = data.stepId;
  }


  save(callback: () => void = () => {}) {
    this.http.post('api/program', {
      steps: this.steps
    })
      .subscribe((value: any) => {
        this.snackBar.open('saved program', 'close', {duration: 1000});
        callback();
      });
  }



  startProgram() {
    this.save(() => {
      this.http.post('api/program/start', {})
        .subscribe((value: any) => {
          this.snackBar.open('starting program', 'close', {duration: 1000});
          this.state = State.RUNNING;

          // reset chart
          this.chartData[0].series = [];
          this.chartData = [...this.chartData];
        });
    });
  }

  pauseProgram() {
    this.http.post('api/program/stop', {})
      .subscribe((value: any) => {
        this.snackBar.open('stopped program', 'close', {duration: 1000});
        this.state = State.PAUSE;
        this.currentStep = -1;
      });
  }


  dateTickFormatting(val: any): string {
    if (val instanceof Date) {
      return (val as Date).toLocaleTimeString('de-DE');
    }
  }
}


enum State {
  PAUSE,
  RUNNING,
}

interface Step {
  id: number
  delay: {
    nanos: number;
    secs: number;
  };
  motorCommand: {
    direction: string
    motorId: number;
    volume: number;
    speed: number;
  };
}
