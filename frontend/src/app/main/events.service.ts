import { Injectable } from '@angular/core';
import {Observable, Observer, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor() { }

  private events: Subject<Event>;

  public connect(url): Subject<Event> {
    if (!this.events) {
      this.events = this.create(url);
      console.log('Successfully connected: ' + url);
    }
    return this.events;
  }


  private create(url): Subject<Event> {
    const ws = new WebSocket(url);

    const observable = Observable.create((obs: Observer<Event>) => {
      ws.onmessage = (msg) => {
        const json = JSON.parse(msg.data);
        obs.next({
          event: json.event,
          data: json.data,
        });
      };
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });
    const observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };
    return Subject.create(observer, observable);
  }
}

export interface Event {
  event: string;
  data: any;
}
