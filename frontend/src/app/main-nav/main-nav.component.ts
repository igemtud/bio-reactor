import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {MatDialog} from '@angular/material';
import {SettingsComponent} from '../settings/settings.component';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {
  motorIds = [0, 1, 2];

  constructor(
    public dialog: MatDialog
  ) {}


  openSettings() {
    const dialog = this.dialog.open(SettingsComponent);
  }
}
