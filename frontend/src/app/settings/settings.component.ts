import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  serialPorts: any[];

  selectedPort: string;
  baudrate = 115200;
  testMsg = '<a, b>';

  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar
  ) {}


  ngOnInit() {
    this.http.get('/api/config/getPorts')
      .subscribe((value: any) => {
        // this.snackBar.open('got value: ' + value.text, 'Close', {duration : 1000});
        this.serialPorts = value;
        console.log('Com: ', value);
        this.selectedPort = value[0].name;
      });
  }


  connect() {
    this.http.post('api/config/reconnectToPort', {
      name: this.selectedPort
    })
      .subscribe((value: any) => {
        this.snackbar.open('connected to serial ', 'Close', {duration : 1000});
        console.log('update: ', value);
      }, error => {
        console.log(error);
        this.snackbar.open('can\'t connect: ' + error.error.error, 'Close', {duration : 5000});
      });
  }


  sendTestMsg() {
    this.http.put('api/config/sendToSerial', this.testMsg)
      .subscribe((value: any) => {
      this.snackbar.open('send test msg: ' + value.msg, 'Close', {duration : 1000});
    });
  }
}


interface MotorSetting {
  id: number;
  speed: number;
  acceleration: number;
  delta: number;
}
