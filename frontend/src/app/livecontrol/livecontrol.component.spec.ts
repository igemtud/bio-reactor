import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivecontrolComponent } from './livecontrol.component';

describe('LivecontrolComponent', () => {
  let component: LivecontrolComponent;
  let fixture: ComponentFixture<LivecontrolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivecontrolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivecontrolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
