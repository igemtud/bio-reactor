import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-livecontrol',
  templateUrl: './livecontrol.component.html',
  styleUrls: ['./livecontrol.component.css']
})
export class LivecontrolComponent implements OnInit {

  runMotorButtonDisabled: boolean;
  motorSettingsButtonDisabled: boolean;
  motorSettings: MotorSetting[] = [
    {
      id: 1,
      speed: 1000,
      acceleration: 200,
      delta: 0.01,
    },
    {
      id: 2,
      speed: 1000,
      acceleration: 200,
      delta: 0.01,
    },
    {
      id: 3,
      speed: 1000,
      acceleration: 200,
      delta: 0.01,
    },
    /*
    {
      id: 4,
      speed: 100,
      acceleration: 200,
      delta: 0.01,
    }
     */
  ];


  runMotor: RunMotor[] = [
    {
      id: 1,
      direction: 'F',
      steps: 1000,
    },
    {
      id: 2,
      direction: 'F',
      steps: 1000,
    },
    {
      id: 3,
      direction: 'F',
      steps: 1000,
    },
    /*
    {
      id: 4,
      direction: 'F',
      steps: 1000,
    }
    */
  ];


  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar
  ) {}


  ngOnInit() {
    this.motorSettingsButtonDisabled = false;
    this.runMotorButtonDisabled = false;
  }

  sendSettings() {
    console.log(this.motorSettings);

    this.http.put('/api/settings/setSettings',
      this.motorSettings,
    ).subscribe((value: any) => {
      this.snackbar.open('send settings: ' + value.msg, 'Close', {duration : 1000});
    });
  }


  sendMoveMotor() {
    console.log(this.runMotor);

    this.http.put('/api/run/moveMotor',
      this.runMotor,
    ).subscribe((value: any) => {
      // this.snackbar.open('run motor: ' + value.msg, 'Close', {duration : 1000});
    });
  }

  sendRunMotor() {
    console.log(this.runMotor);

    this.http.put('/api/run/runAll',
      this.runMotor,
    ).subscribe((value: any) => {
      // this.snackbar.open('run motor: ' + value.msg, 'Close', {duration : 1000});
    });
  }

  sendStop() {
    console.log(this.runMotor);

    this.http.put('/api/run/sendStop',
    1).subscribe((value: any) => {
      // this.snackbar.open('run motor: ' + value.msg, 'Close', {duration : 1000});
    });
  }
}

interface RunMotor {
  id: number;
  direction: string;
  steps: number;
}


interface MotorSetting {
  id: number;
  speed: number;
  acceleration: number;
  delta: number;
}
