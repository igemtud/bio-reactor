# Bio Reactor Control App
App controlled bio reactor.
![](./webapp.gif)

## Build and run
#### Frontend
First install frontend dependencies (you need to have npm installed):
```bash
npm install
```
Build frontend:
```bash
npm run build-install
```

#### Backend
First install dependencies:
```bash
# install rust and set to latest nightly version (skip this step if rust is already installed)
curl https://sh.rustup.rs -sSf | sh
rustup override set nightly
rustup update && cargo update
```
Now start the backend:
```bash
cargo run
```
This will also host the frontend at `http://localhost:8000/`.