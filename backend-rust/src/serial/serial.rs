extern crate serialport;

use serialport::{available_ports, open};
use self::serialport::{SerialPortInfo, Error, SerialPortSettings, SerialPort};
use std::sync::Mutex;
//pub type Result<T> = std::result::Result<T, Error>;
use serde::Serialize;


#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum MotorDirection {
    FORWARDS,
    BACKWARDS
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct MotorMoveCommand {
    pub motorId: i8,
    pub direction: MotorDirection,

    /* mm^2 */
    pub volume: f64,

    /* mm^2/s */
    pub speed: f64,
}

pub struct Serial {
    port: Mutex<Option<Box<dyn SerialPort>>>
}

impl Serial {
    pub fn get_ports() -> Result<Vec<SerialPortInfo>, Error> {
        let ports = available_ports();
        ports
    }

    pub fn reconnect_to_port(&mut self, port_name: String) -> Result<(), String>{
        // unconnect old
        let mut port_guard = self.port.lock().unwrap();
        match port_guard.as_ref() {
            Some(p) => drop(p),
            _ => (),
        }

        // settings for serial connection
        let mut settings: SerialPortSettings = Default::default();
        settings.baud_rate = 115200;

        match serialport::open_with_settings(&port_name, &settings) {
            Ok(mut port) => {
                *port_guard = Some(port);
                println!("connected to serial: {}", port_name);
                Ok(())
            }

            Err(e) => {
                println!("can't connect to serial: {}", port_name);
                Err(e.to_string())
            }
        }
    }

    pub fn send(&mut self, msg: String) -> Result<(), String> {
        let mut port_guard = self.port.lock().unwrap();
        let mut port = port_guard.as_mut().ok_or("serial is not connected")?;

        let bytes = port.write(msg.as_bytes()).map_err(|e| e.to_string())?;
        println!("Written {} bytes to serial: '{}'", bytes, msg);

        let mut gotMsg = String::new();
        let gotBytes = port.read_to_string(&mut gotMsg).map_err(|e| "can't read response from serial: ".to_string() + e.to_string().as_str())?;
        println!("Got Response {} bytes from serial: '{}'", gotBytes, gotMsg);

        Ok(())
    }

    pub fn make() -> Serial {
        Serial {
            port: Mutex::new(None)
        }
    }



    pub fn sendRun(motorIndex: i32, direction: MotorDirection, value: f32) {

    }
}

pub fn t() {

}