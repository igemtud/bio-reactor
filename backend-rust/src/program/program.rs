use std::thread;
use std::time::Duration;
use crate::serial::serial::{MotorMoveCommand, Serial};
use crate::serial::serial::MotorDirection::FORWARDS;
use std::sync::{Mutex, Arc};
use std::borrow::{BorrowMut, Borrow};
use rocket_contrib::json::{Json, JsonValue};
use std::thread::JoinHandle;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;
use crate::EventSender;
use std::rc::Rc;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ProgramStep {
    id: i32,
    delay: Duration,
    motorCommand: MotorMoveCommand,

}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ProgramConfig {
    steps: Vec<ProgramStep>,
}

pub struct InterruptableThread {
    handle: JoinHandle<()>,
    run: AtomicBool,
}

pub struct Program {
    config: Mutex<ProgramConfig>,
    serial: Arc<Mutex<Serial>>,

    threadHandle: Option<JoinHandle<Result<(), ()>>>,
    run: Arc<AtomicBool>,
    eventSender: Arc<EventSender>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum PROGRAM_STATE {
    RUNNING,
    STOPPED,
}


impl Program {
    pub fn new(serial: Arc<Mutex<Serial>>, eventSender: Arc<EventSender>) -> Program {
        Program {
            serial,
            threadHandle: None,
            run: Arc::new(AtomicBool::new(false)),
            eventSender,
            config: Mutex::new(ProgramConfig {
                steps: vec![
                    ProgramStep {
                        id: 0,
                        delay: Duration::from_secs(2),
                        motorCommand: MotorMoveCommand {
                            motorId: 0,
                            direction: FORWARDS,
                            speed: 2.0,
                            volume: 200.0,
                        },
                    },
                    ProgramStep {
                        id: 1,
                        delay: Duration::from_secs(5),
                        motorCommand: MotorMoveCommand {
                            motorId: 1,
                            direction: FORWARDS,
                            speed: 2.0,
                            volume: 200.0,
                        },
                    },
                    ProgramStep {
                        id: 2,
                        delay: Duration::from_secs(6),
                        motorCommand: MotorMoveCommand {
                            motorId: 0,
                            direction: FORWARDS,
                            speed: 2.0,
                            volume: 200.0,
                        },
                    }
                ]
            })
        }
    }


    pub fn update(&mut self, config: ProgramConfig) {
        let mut config_guard = self.config.lock().unwrap();
        *config_guard = config;
    }

    pub fn get(&self) -> ProgramConfig {
        self.config.lock().unwrap().clone()
    }

    pub fn getState(&self) -> PROGRAM_STATE {
        match self.threadHandle.is_none() {
            true => PROGRAM_STATE::STOPPED,
            false => PROGRAM_STATE::RUNNING,
        }
    }

    pub fn start(&mut self) {
        // stop old
        self.stop();

        // sort steps
        {
            println!("will start thread...");
            let mut config_guard = self.config.lock().unwrap();
            config_guard.steps.sort_by(|a, b| a.delay.cmp(&b.delay));

            //println!("sorted steps {:?}", config_guard.steps);
        }

        // perform steps
        // copy needed vars
        let config = self.config.lock().unwrap().clone();
        let mut serial = self.serial.clone();
        let mut run = self.run.clone();
        run.store(true, Relaxed);
        let eventSender = self.eventSender.clone();

        let thread = thread::spawn(move || -> Result<(), ()> {

            // start event
            eventSender.sendEvent("program/state", json!({
                "state": "started"
            }));

            let mut waitedTime = Duration::from_micros(0);
            for s in config.steps.iter() {
                // wait till next step
                let sleepTime = s.delay - waitedTime;
                waitedTime+= sleepTime;
                println!("step delay {:?}, will wait for {:?}", s.delay, sleepTime);

                //thread::sleep(waitedTime);
                interruptableSleep(sleepTime, run.clone())?;

                //println!("wait finished \n");

                // event
                eventSender.sendEvent("program/state", json!({
                "state": "step",
                "stepId": s.id,
                }));

                // send command to serial
                let mut serial_guard = serial.lock().unwrap();
                match serial_guard.borrow_mut().send("adsf".to_string()) {
                    Err(e) => println!("while executing program: can't send to serial: {}", e),
                    _ => (),
                }

            }
            Ok(())
        });
        self.threadHandle = Some(thread);
    }

    pub fn stop(&mut self) {
        let thread = self.threadHandle.take();

        match thread {
            Some(thread) => {
                self.run.store(false, Relaxed);
                thread.join();
                print!("stopped program");
            }

            _ => print!("no program running")
        }
    }
}

fn interruptableSleep(dur: Duration, cond: Arc<AtomicBool>) -> Result<(), ()> {
    let sleepPeriod = Duration::from_millis(100);
    let times = dur.as_micros() / sleepPeriod.as_micros();
    for _ in 0 .. times {
        if cond.load(Relaxed) {
            thread::sleep(sleepPeriod);
        } else {
            println!("will interrupt wait");
            return Err(());
        }
    }
    Ok(())
}