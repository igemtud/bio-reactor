use rocket::State;
use crate::AppState;
use rocket::response::status::NotFound;
use rocket_contrib::json::{JsonValue, Json};
use crate::program::program::ProgramConfig;

#[post("/program", data = "<config>")]
pub fn post_program(mut state: State<AppState>, config: Json<ProgramConfig>) -> Result<(), NotFound<JsonValue>> {
    println!("got: {:?}", config.0);

    state.program.lock().unwrap().update(config.0);
    Ok(())
}

#[get("/program")]
pub fn get_program(mut state: State<AppState>) -> Result<JsonValue, NotFound<JsonValue>> {
    Ok(json!(
        state.program.lock().unwrap().get()
    ))
}

#[get("/program/state")]
pub fn get_program_state(mut state: State<AppState>) -> Result<JsonValue, ()> {
    let state = state.program.lock().unwrap().getState();
    Ok(json!({
        "state": state
    }))
}

#[post("/program/start")]
pub fn post_program_start(mut state: State<AppState>) -> Result<(), NotFound<JsonValue>> {
    state.program.lock().unwrap().start();
    Ok(())
}

#[post("/program/stop")]
pub fn post_program_stop(mut state: State<AppState>) -> Result<(), NotFound<JsonValue>> {
    state.program.lock().unwrap().stop();
    Ok(())
}


fn resultToHttpError<T>(r:  Result<T, String>) -> Result<T, NotFound<JsonValue>> {
    match r {
        Err(error) => Err(NotFound(json!({
                "error": error
            }))),
        Ok(o) => Ok(o)
    }
}