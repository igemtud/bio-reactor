use crate::serial::serial::Serial;
use serde::Serialize;
use rocket_contrib::json::{JsonValue, Json};
use rocket::response::status::{NotFound, Custom};
use crate::AppState;
use rocket::State;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Port {
    name: String,
}

#[get("/config/getPorts")]
pub fn get_ports() -> JsonValue {
    let ports = Serial::get_ports().expect("no serial ports found");
    let ports_mapped: Vec<Port> = ports.into_iter().map(|p| {
        Port {
            name: p.port_name,
        }
    }).collect();

    json!(ports_mapped)
}

#[post("/config/reconnectToPort", data = "<device>")]
pub fn get_reconnectToPort(mut state: State<AppState>, device: Json<Port>) -> Result<(), NotFound<JsonValue>> {
    println!("will connect to {:?}", device);
    resultToHttpError(
        state.serial.lock().unwrap().reconnect_to_port(device.name.clone())
    )
}

#[put("/config/sendToSerial", data = "<msg>")]
pub fn get_sendToSerial(mut state: State<AppState>, msg: String) -> Result<(), NotFound<JsonValue>> {
    resultToHttpError(
        state.serial.lock().unwrap().send(msg)
    )
}


fn resultToHttpError<T>(r:  Result<T, String>) -> Result<T, NotFound<JsonValue>> {
    match r {
        Err(error) => Err(NotFound(json!({
                "error": error
            }))),
        Ok(o) => Ok(o)
    }
}