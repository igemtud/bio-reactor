use pyo3::prelude::*;
use pyo3::types::{IntoPyDict, PyAny};
use serde::{Deserialize, Serialize};
use std::rc::Rc;
use std::thread::JoinHandle;
use std::thread;
use crate::EventSender;
use std::sync::Arc;
use std::time::Duration;


pub struct OdSensor {
    thread_handle: Option<JoinHandle<Result<(), ()>>>,
}

impl OdSensor {

    pub fn new() -> OdSensor {
        OdSensor {
            thread_handle: None,
        }
    }

    pub fn start(&mut self, eventSender: Arc<EventSender>) {
        let thread = thread::spawn(move || -> Result<(), ()> {
            // python module for od sensor
            let gil = (Python::acquire_gil());
            let py = (gil.python());
            // program will panic if module can't be created
            let module = makeModule(py).map_err(|e| mapPythonError(e, py)).unwrap();

            // connect to sensor
            let connected = connectSensor(py, module).map_err(|e| "Can't connect to odSensor: ".to_string() + e.as_str());
            match connected.clone()  {
                Err(e) => eprintln!("[FATAL] {} \n[FATAL] -> please restart backend to retry! \n[FATAL] -> will run backend without odSensor... \n[FATAL] ---> will use dummy data for odSensor", e),
                Ok(_) =>           println!(" [OK]  connected to odSensor"),
            };


            // get sensor values
            loop {
                // read values
                match readDummyOrRealSensorValues(py, module, connected.clone().is_ok()) {
                    Ok(value) => eventSender.sendEvent("odSensor/data", json!(value)),
                    Err(e) => eprintln!("[ERROR] can't read od sensor values: {}", e),

                }

                thread::sleep(Duration::from_secs(1));
            }
        });
        self.thread_handle = Some(thread);
    }
}


fn readDummyOrRealSensorValues(python: Python, module: &PyModule, useRealSensor: bool) -> Result<CalibratedValues, String> {
    if useRealSensor {
        return readSensorValues(python, module);
    } else {
        return readDummySensorValues(python, module);
    }
}


pub fn makeModule<'p>(py: Python<'p>) -> PyResult<&'p PyModule> {

    // define python code for calling as7262
    let module = PyModule::from_code(py, "

from as7262 import AS7262, CalibratedValues
import time
import json
import random

as7262: AS7262 = None

def initSensor():
    global as7262

    try:
        as7262 = AS7262()
        as7262.soft_reset()

        # Set Gain multiplier one of 1, 3.7, 16 or 64
        as7262.set_gain(3.7)

        # Integration time 0 - 255 (Integrationtime *2.8)
        as7262.set_integration_time(35)

        # Measurement Mode
                # 0: Continuous reading of VBGY (Visible) / STUV (IR)
                # 1: Continuous reading of GYOR (Visible) / RTUX (IR)
                # 2: Continuous reading of all channels
                # 3: One-shot reading of all channels (power-on default)
        as7262.set_measurement_mode(2)

        # LED on / off
        as7262.set_illumination_led(0)


    except OSError as e:
        print(e)
        return str(e)
    return ''


def readValues():
    global as7262
    values = as7262.get_calibrated_values()
    return json.dumps(values.__dict__)


# for testing without real sensor
x = 0
def genTestValues():
    global x
    v = random.random()*x
    #print(v)
    values = CalibratedValues(v, v, v, v, v, v)
    x = x+0.05
    return json.dumps(values.__dict__)


    ", "odSensor", "odSensor");
    module
}

#[pyclass]
#[derive(Debug, Serialize, Deserialize)]
pub struct CalibratedValues {
    red: f32,
    orange: f32,
    yellow: f32,
    green: f32,
    blue: f32,
    violet: f32,
}

pub fn readSensorValues(python: Python, module: &PyModule) -> Result<CalibratedValues, String> {
    let f = || -> PyResult<CalibratedValues> {
        let result: String = module.call0("readValues")?.extract()?;
        let v: CalibratedValues = serde_json::from_str(result.as_str()).unwrap();
        Ok(v)
    };
    f().map_err(|e| mapPythonError(e, python))
}

pub fn readDummySensorValues(python: Python, module: &PyModule) -> Result<CalibratedValues, String> {
    let f = || -> PyResult<CalibratedValues> {
        let result: String = module.call0("genTestValues")?.extract()?;
        let v: CalibratedValues = serde_json::from_str(result.as_str()).unwrap();
        Ok(v)
    };
    f().map_err(|e| mapPythonError(e, python))
}

pub fn connectSensor(python: Python, module: &PyModule) -> Result<(), String> {
    let f = || -> PyResult<String> {
        let result: String = module.call0("initSensor")?.extract()?;
        let err = result.as_str();
        /*
        println!("[{}] result from python {}", "t", result);
        println!("[{}] result from python {:?}", "t", v);
        */

        Ok(String::from(err))

    };

    let err = f().map_err(|e| mapPythonError(e, python))?;
    if err.len() <= 0 {
        return Ok(())
    } else {
        return Err(err)
    }
}


pub trait FromPyObject<'source>: Sized {
    /// Extracts `Self` from the source `PyObject`.
    fn extract(ob: &'source PyAny) -> PyResult<Self>;
}


pub fn mapPythonError(error: PyErr, py: Python) -> String {
    printPythonError(error, py);
    "python error".into()
}

fn printPythonError(error: PyErr, py: Python) {
    // We can't display python error type via ::std::fmt::Display,
    // so print error here manually.
    //e.print(py);
    error.print_and_set_sys_last_vars(py);
}
