use crate::{sensors, EventSender};
use crate::program::program::Program;
use crate::serial::serial::Serial;
use std::sync::{Arc, Mutex};
use std::borrow::BorrowMut;

#[test]
fn testPython() {
    //sensors::ODSensor::initODSensor();
}


#[test]
fn testThread() {
    let mut ws = Arc::new(EventSender::new());
    ws.borrow_mut().startWsServer();
    let serial = Arc::new(Mutex::new(Serial::make()));
    let mut program = Program::new(serial.clone(), ws.clone());

    program.start();
}