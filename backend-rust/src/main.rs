#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_contrib;
use rocket_cors;

#[macro_use] extern crate serde_derive;

use serialport::SerialPortInfo;
use crate::serial::serial::Serial;
use std::sync::{Mutex, Arc};
use rocket_contrib::json::JsonValue;
use serde::Serialize;
use crate::program::program::Program;
use ws::{listen, Handler, Message, CloseCode, Error, Sender};
use std::thread;
use std::thread::{Thread, JoinHandle};
use std::time::Duration;
use std::rc::Rc;
use std::borrow::BorrowMut;
use pyo3::{Python, Py};
//use crate::sensors::ODSensor::{makeModule, mapPythonError, readSensorValues};
use crate::sensors::ODSensor::OdSensor;
use pyo3::prelude::PyModule;
use rocket_contrib::serve::StaticFiles;

pub mod serial;
pub mod sensors;
pub mod routes;
pub mod program;

pub mod tests;

pub struct EventSender {
    wsSender: Arc<Mutex<Vec<Sender>>>,
    threadHandle: Mutex<Option<JoinHandle<()>>>,
}

impl EventSender {
    pub fn new() -> EventSender {
        EventSender {
            wsSender:  Arc::new(Mutex::new(Vec::new())),
            threadHandle: Mutex::new(None),
        }
    }

    pub fn startWsServer(& self) {
        let mut wsSenderCopy = self.wsSender.clone();

        // websocket
        let webSocketThread = thread::spawn(move|| {
            let r = listen("localhost:9400", |out: Sender| {
                println!("new ws connection");
                {
                    let mut wsS = wsSenderCopy.lock().unwrap();
                    wsS.push(out.clone());
                }

                // on_message
                move |msg| {
                    println!("ws got: {}", msg);
                    Ok(())
                }
            });

            match r {
                Err(e) => println!("can't create websocket server: {}", e.to_string()),
                Ok(_0) => println!("created websocket server"),
            }
        });

        *self.threadHandle.lock().unwrap() = Some(webSocketThread);
    }

    pub fn sendEvent(& self, event: &str, data: JsonValue) {
        let l = self.wsSender.lock().unwrap();
        for x in l.iter() {
            x.send(json!({
                "event": event,
                "data": data,
            }).to_string());
        }
    }

}


#[get("/info")]
fn get_info() -> JsonValue {
    json!({
        "version": "0.0.1"
    })
}

pub struct AppState {
    serial: Arc<Mutex<Serial>>,
    program: Mutex<Program>,
    odSensor: Arc<Mutex<OdSensor>>,
}

/*
struct OdSensor<'a> {
    python: Python<'a>,
    module: &'a PyModule,
}
*/


fn main() {
    println!("start bioreactor server: {}", serde_json::to_string(&get_info()).unwrap());


    /*
    // python module for od sensor
    let gil = (Python::acquire_gil());
    let py = (gil.python());
    // program will panic if module can't be created
    let module = makeModule(py).map_err(|e| mapPythonError(e, py)).unwrap();

    let od = Arc::new((OdSensor {
        python: py,
        module
    }));

    readSensorValues(py, module);
    */

    // state
    let mut ws = Arc::new(EventSender::new());
    let odSensor = Arc::new(Mutex::new(OdSensor::new()));
    let serial = Arc::new(Mutex::new(Serial::make()));
    let program = Mutex::new(Program::new(serial.clone(), ws.clone()));

    // start websocket thread and sensor thread
    {
        ws.startWsServer();
        odSensor.lock().unwrap().start(ws.clone());
    }

    let state = AppState{
        serial,
        program,
        odSensor,
    };



    // start rest server
    rocket::ignite()
        .mount("/", StaticFiles::from("web"))
        .mount("/api", routes![
            get_info,
             routes::config::get_ports,
             routes::config::get_reconnectToPort,
             routes::config::get_sendToSerial,
             routes::program::post_program,
             routes::program::get_program,
             routes::program::get_program_state,
             routes::program::post_program_start,
             routes::program::post_program_stop,
        ])
        .manage(state)
        .launch();
}
